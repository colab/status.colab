/// <reference path="typings/node/node.d.ts"/>
/// <reference path="typings/express/express.d.ts"/>
/// <reference path="typings/mongodb/mongodb.d.ts"/>
/// <reference path="typings/request/request.d.ts"/>

"use strict";
import http = require('http');
import mongo = require('mongodb');
	var mongoClient = mongo.MongoClient;
import request = require('request');

var acceptableStatusValues : string[] = [//acceptable values for status
	"running",
	"issues",
	"critical",
	"maintainance",
	"unavailable",
	"crashed",
	"failed"
];

var trees : StatusTree[] = [];//use names as string indices
var services : string[] = [];//use to track the services to pull statuses from
	
//Set up DB
//-------------------------
	var db : mongo.Db;
	
	mongoClient.connect('mongodb://127.0.0.1:27017/status',{
		'server': {'socketOptions': {'autoReconnect':true}}//wow that's buried deep in mongo2.0, and pretty different from most stuff a google search will get ya	
	},function(err : Error, _db : mongo.Db) {
		if (err) {
			//TODO fail and die
		} else {
			db = _db;
			db.collection('_update_urls').find().forEach(function(doc) {
				if (doc.name && doc.url) {
					services[doc.name] = doc.url;//when we start up, load up all the urls we want to be checking on!
				}
			},
			function(err : Error) {
				//be sad
			});
		}
	});

//---------------------------
//Set up checking for updates
//---------------------------

setInterval(function() {
	services.forEach(function (url,name) {//for each service
		request(url,function(name : string, error : any, res : http.IncomingMessage, body: any) : void {//pull down its status url
			if (error) {//if there was an error pulling 
				addStatus({//add an unavailable status
					'name': name,
					'children': [],
					'status': 'unavailable',
					'timeStamp': Number(new Date()),
					'stats': {},
					'description': trees[name].status == 'unavailable' ? trees[name].description : "Became unavailable @ "+new Date().toISOString()
				});
				return;
			}
			if (body == 'STOP') {//if they ask us to stop
				services.splice(services.indexOf(name),1);//stop tracking this url
				return;
			}
			addStatus(JSON.parse(body));//otherwise, add the parsed status data
		}.bind(null,name));
	});
},60*1000);//once per minute








export function toJSON() : string {
	function removeStatsFromString(key : string,value : any) {//called from stringify 
		if (key === 'stats') {
			return undefined; 
		}
		return value;
	}
	
	return JSON.stringify(trees,removeStatsFromString);//stringify all our most recent data
}

export function addStatus(potential : StatusTree) : boolean {//returns success value
	var st : StatusTree = {
		name: '',
		status: '',
		description: '',
		timeStamp: 0,
		stats: '',
		children: [] 
	};//ensure the object contains only expected things in acceptable lengths
	
	if (acceptableStatusValues.indexOf(potential.status) > -1) {
		st.status = potential.status;//ensure that the right status values are used
	} else {
		return false;//fail to add status
	}
	
	st.name = potential.name.substring(0,32);//max length
	if (name == '_update_urls') {return false;}//make sure we're not writing documents to the update url collection
	st.description = potential.description.substring(0,256);
	st.timeStamp = Number(potential.timeStamp);//should just be a number
		if (!(st.timeStamp > 0)) {st.timeStamp = Number(new Date());}
	st.stats = potential.stats;
	st.children = potential.children;//just pray that limiting the size of stats and children isn't a problem
	

	if (db) {//add st to the db
		db.collection(st.name).insertOne(st,function(err : Error, result: any) {
			//TODO report errors, also figure out a way of reporting success
		});
	}
	trees[st.name] = st;
	
	return true;
}

export function getDetails(res: http.ServerResponse, name : string, records? : number, until? : number) : void {//good for constantly querying updates on a particular tree or pulling down a large number of records
	if (!records && !until) {res.end(trees[name] ? JSON.stringify(trees[name]) : '{}'); return;}
	//just give details about the current status then (basically just include the stats object)
	if (!records) {records = 1;}
	if (!until) {until = Number(new Date());}
	var result : any[] = [];
	if (db) {
		db.collection(name).find({'timeStamp': {$lt: until}}).sort({'timestamp':-1}).forEach(function(doc : any) : void {//get the data from this collection whose dates are before _until_ and sort it descending by timestamp
			result.push(doc);
		}, function (err) {
			if (err != null) {res.statusCode = 500; res.end('Error pulling from DB');return;}
			res.end(JSON.stringify(result));
		});
	} else {
		res.statusCode = 500;
		res.end('No DB Connection');
	}
}

export function offerStatus(res : http.ServerResponse, name : string, url : string) : void {
	//add a new status to track
	if (services[name]) {res.statusCode = 409; res.end('Already have a service with that name!'); return;}
	services[name] = url;
	db.collection('_update_urls').insertOne({'name': name, 'url': url},function (err : Error, res: any) : void {
		if (!err) {res.statusCode = 500; res.end('Error inserting to DB');return;}
		res.end();
	});
}


interface StatusTree {
	name : string;
	status : string;
	children : StatusTree[];
	description : string;
	timeStamp : number;
	stats : any;
}

