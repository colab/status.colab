/// <reference path="typings/node/node.d.ts"/>
/// <reference path="typings/express/express.d.ts"/>

//status.colab Server

import http = require('http');
import https = require('https');
import path = require('path');
import url = require('url');  
import fs = require('fs');
import express = require('express');
import statuses = require('./statusManager');

//Switch to strict mode to make stuff fastahhhh
"use strict"; 

var app = express();

app.use('/',express.static('static'));//serve up static files from the root

//does what it says: redirects http requests to https. EVERYTHING SHOULD BEEEE HTTTPSSSSSSS
var httpToHttpsRedirector : http.Server = http.createServer(function(req : http.IncomingMessage, res : http.ServerResponse) {
	res.writeHead(301,{'Location': 'https://'+req.headers.host+req.url});
	res.write("Redirecting to https://"+req.headers.host+req.url);
	res.end();
}).listen(80);

//MAKES A SERVER. Options are also helpful
var options : https.ServerOptions = {
  		key: fs.readFileSync('ssl/status.colab.key'),
  		cert: fs.readFileSync('ssl/status.colab.cert')
};
var server : https.Server = https.createServer(options,app).listen(443);//creates https express app

app.get('/status',function(req : express.Request, res : express.Response) {
	//json syndicated object of current status info
	res.writeHead(200, {"Content-Type": "application/json"});
	res.end(statuses.toJSON());
});

app.get('/status/:name',function(req : express.Request, res : express.Response) {
	//json object describing detailed and temporally graphed data about a service
	statuses.getDetails(res,req.params.name,Number(req.query.count),Number(req.query.until));
});

app.post('/status',function(req : express.Request, res : express.Response) {
	//allow apps to request updates be pulled
	statuses.offerStatus(res,req.body.name,req.body.url);
});