# status.colab #

This is a fairly simple server whose job it is to monitor the status of the various co-lab and co-lab compliant services, as well as to provide alerts and display information with regards to these statuses.

In contrast to most other Co-Lab services, this one is entirely self-contained so as to provide monitoring support for services that it would otherwise depend on, like 

### Compliance ###

To build a Co-Lab compliant service for status monitoring, you must provide an API endpoint for this system to monitor. This endpoint must be accessible by GET request, and will be syndicated into the Co-Lab API system for access. 

```Javascript
{
	name: 'service name',
	status: 'running',
	children: [child_status_objects, ... ],
	description: 'All tests succeeded, no issues detected. no issues in the last 10 days.'
	timestamp: millisecs since 1970 (javascript uses (new Date()).getTime(); )
	stats: {arbitrary stats object, should be self-consistent and easy to plot}
}
```
Stats Object Example
These objects are completely arbitrary and can be used to track additional information specific to one service
```Javascript
{
	reqsPerSecond: 3494,
	reqAverageTime: 120,
	dbSize: 192837498
}
```

Feel free to add as many child statuses as you would like to give a high fidelity to the status information that you're providing.

##### Acceptable Values for status property #####

To make rendering and use of the statuses, as well as alerts, most effective, the possible values for _status_ have been restricted. Below is a list of them and the meanings they convey.

- running: No issues to report, service operating as usual
- issues: Service is running as expected, but non-critical issues have occurred. Users of this service shouldn't be affected, but the issues should still be addressed.
- critical: The service is operating in some capacity, but parts of it are damaged and will affect usage of this service. 
- maintainance: The service is undergoing maintainance, which may impact user experience, but this is expected.
- unavailable: This indicates that a service could not be contacted, so its operational status is unknown (but very likely failed, and in many cases the two are equally bad)
- failed: The service is not operational in any capacity and users are unable to use it.  
- crashed: The same as failed.

You are welcome to evaluate your own status however you might like, but the more descriptive and comprehensive your status-monitorinig code is, the more robust and reliable your service will be!

### Adding your Service ###

Once you have fulfilled the compliance requirement (not exactly hard), your service must be added as a child to an existing service. Services added to api.colab, for example, are automatically added as its children. You may also add your service as a child here or at (dev.colab)[https://dev.colab.duke.edu].