var statusAPI = (function() {
	var d = document;
	var trees : StatusTree[][] = [];
	
	function offerTree(name : string, url : string, callback : (success : boolean) => void) : void {
		//call POST status.colab.duke.edu/status name=name, url=url and then call the callback
	}
	
	function updateDetails(t : StatusTree) : void {
		//call GET status.colab.duke.edu/status/:name and store the tree results in trees[name][#]
		d.dispatchEvent(d.createEvent('updatedDetails'));
		
	}
	
	function updateAll() : void {
		//call GET status.colab.duke.edu/status and store tree results in trees[name]
		d.dispatchEvent(d.createEvent('updatedAll'));
	}
	
	return {
		'offerTree': offerTree,
		'updateAll': updateAll,
		'updateDetails': updateDetails,

	}
})();

interface StatusTree {
	name : string;
	status : string;
	children : StatusTree[];
	description : string;
	timeStamp : number;
	stats : any;
}
