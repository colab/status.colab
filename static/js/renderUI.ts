/// <reference path="statusAPI.ts"/>


var renderUI = (function (statusAPI) {
	var d = document;
	var elements = {
		withClass : d.getElementsByClassName,
		withId : d.getElementById
	}
	var w = window;
	
	elements.withClass('smallStatus');
	elements.withClass('statusList');//add event listeners (update events)
	elements.withClass('fullStatus');
	elements.withClass('statusOfferButton');
	
	setTimeout(statusAPI.updateAll, 2000);//update every few seconds
	
	function renderSmall(el : HTMLElement, status : StatusTree) : void{
		//run templating engine over status and insert into el
	}
	function renderFull(el : HTMLElement, status : StatusTree) : void {
		
	}
	function renderListItem(el : HTMLElement, status : StatusTree) : void {
		renderSmall(el,status);//let's keep it simple for now and make these the same
	}
	function renderList(el : HTMLElement, status : StatusTree) : void {
		
	}
	return {
		'renderSmall': renderSmall,
		'renderFull': renderFull,
		'renderListItem': renderListItem,
		'renderList': renderList
	};
})(statusAPI);